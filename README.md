# Projekt zaliczeniowy z systemów wielowarstwowych

## Wymagania:

* Możliwość utworzenia konta w aplikacji oraz zalogowania.
* Do potwierdzenia tożsamości użytkownika wykorzystywany jest JSON Web Token.
* Każdy użytkownik ma własną ToDo listę i tylko on ma do niej dostęp.
* Operacje związane z zadaniami:
* Dodawanie zadań na listę.
* Edycja zadań z listy.
* Kasowanie zadań z listy.
* Oznaczanie zadania jako gotowe.
* Użytkownik ma mieć możliwość dodawania terminu wykonania zadania. Zbliżenie się
do tego terminu oraz jego przekroczenie powinno zostać w jakiś sposób
zasygnalizowane.
* Aplikacja musi wykorzystywać bazę danych
* Każda warstwa aplikacji powinna być uruchamiana jako oddzielny kontener.

W aplikacji dodatkowo istnieje podział na listy zadań.

## Uruchomienie projektu

Należy pobrać repozytorium, a następnie w katalogu głównym projektu uruchomić polecenie:
`docker-compose up -d`

Projekt składa się z dwóch aplikacji:
* frontend - `http://localhost:3000`
* backend - `http://localhost:8000`

API posiada dokumentację OpenAPI pod `http://localhost:8000/api`
