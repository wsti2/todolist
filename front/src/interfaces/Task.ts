export default interface Task {
    id: string;
    toDoList: string;
    name: string;
    description: string;
    isCompleted: boolean;
    deadlineDate: Date|null;
    createdAt: Date;
}
