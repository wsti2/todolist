export default interface ToDoList {
    id: string;
    owner: string;
    name: string;
}
