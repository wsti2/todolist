export default interface ToDoListFormData {
    id?: string;
    name: string;
}
