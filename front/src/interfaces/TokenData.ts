export default interface TokenData {
    iat: string;
    exp: string;
    id: string;
    username: string;
}
