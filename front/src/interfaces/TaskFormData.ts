export default interface TaskFormData {
    id?: string;
    todoList: string;
    name: string;
    description: string;
    isCompleted: boolean;
    deadlineDate: Date|null|string;
}
