export default interface Token {
    token: string;
    refresh_token: string;
}
