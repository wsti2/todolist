import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import { App } from './App';
import { Login } from './components/login/Login';
import { Register } from './components/register/Register';
import { Dashboard } from "./components/dashboard/Dashboard";
import {RequireAuth} from "./components/require-auth/RequireAuth";
import {ToDoList} from "./components/to-do-list/ToDoList";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <BrowserRouter>
          <Routes>
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route element={<RequireAuth />}>
                  <Route element={<App />} >
                      <Route path="/" element={<Dashboard />} />
                      <Route index path="dashboard" element={<Dashboard />} />
                      <Route path="to-do-lists/:toDoListId" element={<ToDoList />} />
                  </Route>
              </Route>
          </Routes>
      </BrowserRouter>
  </React.StrictMode>
);
