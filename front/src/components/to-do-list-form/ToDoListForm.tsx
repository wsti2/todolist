import React, {useState} from "react";
import {useFormik} from "formik";
import * as Yup from "yup";
import {Form} from "../form/Form";
import {FormField} from "../form-field/FormField";
import {Button} from "../button/Button";
import {createToDoList, editToDoList} from "../../services/ToDoListService";
import ToDoList from "../../interfaces/ToDoList";
import ToDoListFormData from "../../interfaces/ToDoListFormData";

interface ToDoListFormFields {
    name: string;
}

interface ToDoListFormProps {
    closeModal: () => void,
    onSubmit: (toDoList: ToDoList) => void,
    toDoList?: ToDoList,
}

export const ToDoListForm: React.FC<ToDoListFormProps> = ({ closeModal, onSubmit, toDoList }) => {
    const [errors, setErrors] = useState('');

    const formik = useFormik({
        initialValues: {
            name: toDoList?.name || '',
        } as ToDoListFormFields,
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'Must be 3 characters or more')
                .max(20, 'Must be 20 characters or less')
                .required('Required'),
        }),
        async onSubmit(values: ToDoListFormFields) {
            setErrors('');
            const toDoListObject: ToDoListFormData = {
                name: values.name,
            }
            if (toDoList) {
                toDoListObject['id'] = toDoList.id;
            }
            const response = toDoList?.id ? await editToDoList(toDoListObject) : await createToDoList(toDoListObject);
            if (response) {
                onSubmit(response as ToDoList);
                closeModal();
                return;
            }
            setErrors('An error occurred during submission.');
            return;
        }
    });

    return (
        <div className="h-full flex justify-center items-center">
            <div className="h-min rounded-md drop-shadow-2xl">
                <h1 className="text-3xl text-center font-bold mb-8">
                    { toDoList?.id ? 'Edit list' : 'Add list' }
                </h1>
                { errors &&
                    <div className="p-3 bg-red-500 rounded mb-3 text-center text-white">
                        {errors}
                    </div>
                }
                <Form onSubmit={formik.handleSubmit}>
                    <FormField
                        name="name"
                        label="Name"
                        type="text"
                        value={formik.values.name}
                        required={true}
                        error={formik.errors.name}
                        valid={formik.touched.name && formik.errors.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <div className="grid grid-col gap-4">
                        <Button type="submit">
                            { toDoList?.id ? 'Edit' : 'Add' }
                        </Button>
                    </div>
                </Form>
            </div>
        </div>
    )
}
