import React, {useEffect, useState} from "react";
import Task from "../../interfaces/Task";
import {getTask} from "../../services/TaskService";

export interface TaskModalContentProps {
    taskId: string;
}

export const TaskModalContent: React.FC<TaskModalContentProps> = ({ taskId }) => {
    const [task, setTask] = useState<Task|null>(null);

    useEffect(() => {
        getTask(taskId)
            .then(task => {
                setTask(() => task as Task)
            });
    }, []);

    return (
        <div>
            <div className="mb-4 w-96">
                <h2 className="text-2xl font-bold mb-4">{task?.name}</h2>
                <p className="mb-2">
                    <span className="pr-2">Description: </span>
                    <span className="italic">{task?.description ? task?.description : 'No description'}</span>
                </p>
                <p className="mb-2">
                    <span className="pr-2">Deadline date: </span>
                    <span className="italic">
                        { task?.deadlineDate
                            ? (new Date(task?.deadlineDate)).toLocaleDateString()
                            : 'No deadline'
                        }
                    </span>
                </p>
                <p className="mb-2">
                    <span className="pr-2">Is completed: </span>
                    <span className="italic">{task?.isCompleted ? 'Yes' : 'No'}</span>
                </p>
            </div>
        </div>
    )
}
