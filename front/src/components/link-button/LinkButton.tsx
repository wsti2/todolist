import {Link} from "react-router-dom";
import {Button} from "../button/Button";
import React from "react";

export interface LinkButtonProps {
    link: string;
    children: string;
}

export const LinkButton: React.FC<LinkButtonProps> = ({ children, link }) => {
    return (
        <Link to={link} className="grid">
            <Button type="button">
                {children}
            </Button>
        </Link>
    )
}
