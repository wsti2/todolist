import React, { ReactNode } from "react";

export interface ButtonProps {
    type: "submit"|"button"|"reset"|undefined;
    children?: ReactNode|ReactNode[];
    onClick?: () => void
}

export const Button: React.FC<ButtonProps> = ({ type, children, onClick }) => {
    return (
        <button
            onClick={onClick}
            className="bg-orange-500 hover:bg-orange-600 text-white p-3 rounded font-medium uppercase tracking-wider flex gap-2 justify-center"
            type={type}
        >
            {children}
        </button>
    )
}
