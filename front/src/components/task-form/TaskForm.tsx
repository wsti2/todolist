import React, {useState} from "react";
import {useFormik} from "formik";
import * as Yup from "yup";
import {Form} from "../form/Form";
import {FormField} from "../form-field/FormField";
import {Button} from "../button/Button";
import {createTask, editTask} from "../../services/TaskService";
import Task from "../../interfaces/Task";
import TaskFormData from "../../interfaces/TaskFormData";
import {TextareaField} from "../textarea-field/TextareaField";
import {CheckboxField} from "../checkbox-field/CheckboxField";
import {Datepicker} from "../datepicker/Datepicker";

interface TaskFormFields {
    name: string;
    description: string;
    isCompleted: boolean;
    deadlineDate: Date|null;
}

interface TaskFormProps {
    closeModal: () => void,
    onSubmit: (task: Task, toDoListId: string) => void,
    task?: Task,
    toDoListId: string,
}

export const TaskForm: React.FC<TaskFormProps> = ({ closeModal, onSubmit, task, toDoListId }) => {
    const [errors, setErrors] = useState('');

    const formik = useFormik({
        initialValues: {
            name: task?.name || '',
            description: task?.description || '',
            isCompleted: task?.isCompleted || false,
            deadlineDate: task?.deadlineDate || null
        } as TaskFormFields,
        validationSchema: Yup.object({
            name: Yup.string()
                .min(3, 'Must be 3 characters or more')
                .max(20, 'Must be 20 characters or less')
                .required('Required'),
            description: Yup.string(),
            isCompleted: Yup.boolean(),
            deadlineDate: Yup.date()
                .when('isCompleted', {
                    is: false,
                    then: Yup.date().min(new Date(), 'Please choose future date'),
                }),
        }),
        async onSubmit(values: TaskFormFields) {
            setErrors('');
            const taskObject: TaskFormData = {
                todoList: '/api/todo_lists/' + toDoListId,
                name: values.name,
                description: values.description,
                isCompleted: values.isCompleted,
                deadlineDate: values.deadlineDate?.toLocaleString() ?? null
            }
            if (task) {
                taskObject['id'] = task.id;
            }
            const response = task?.id ? await editTask(taskObject) : await createTask(taskObject);
            if (response) {
                onSubmit(response as Task, toDoListId);
                closeModal();
                return;
            }
            setErrors('An error occurred during submission.');
            return;
        }
    });

    return (
        <div className="h-full flex justify-center items-center">
            <div className="h-min rounded-md drop-shadow-2xl">
                <h1 className="text-3xl text-center font-bold mb-8">
                    { task?.id ? 'Edit task' : 'Add task' }
                </h1>
                { errors &&
                    <div className="p-3 bg-red-500 rounded mb-3 text-center text-white">
                        {errors}
                    </div>
                }
                <Form onSubmit={formik.handleSubmit}>
                    <FormField
                        name="name"
                        label="Name"
                        type="text"
                        value={formik.values.name}
                        required={true}
                        error={formik.errors.name}
                        valid={formik.touched.name && formik.errors.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <TextareaField
                        name="description"
                        label="Description"
                        value={formik.values.description}
                        required={false}
                        error={formik.errors.description}
                        valid={formik.touched.description && formik.errors.description}
                        onChange={formik.handleChange}
                    />
                    <Datepicker
                        name="deadlineDate"
                        label="Deadline date"
                        required={false}
                        value={formik.values.deadlineDate}
                        error={formik.errors.deadlineDate}
                        valid={formik.touched.deadlineDate && formik.errors.deadlineDate}
                        onChange={(date: any) => formik.setFieldValue('deadlineDate', date)}
                    />
                    {
                        task?.id ? <CheckboxField
                            name="isCompleted"
                            label="Is completed?"
                            required={false}
                            error={formik.errors.isCompleted}
                            valid={formik.touched.isCompleted && formik.errors.isCompleted}
                            onChange={formik.handleChange}
                        /> : ''
                    }
                    <div className="grid grid-col gap-4">
                        <Button type="submit">
                            { task?.id ? 'Edit' : 'Add' }
                        </Button>
                    </div>
                </Form>
            </div>
        </div>
    )
}
