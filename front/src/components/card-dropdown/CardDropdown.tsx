import React, {useState} from "react";

interface CardDropdownProps {
    children: React.ReactNode | React.ReactNode[]
}

export const CardDropdown: React.FC<CardDropdownProps> = ({ children }) => {
    const [showMenu, setShowMenu] = useState(false);

    return (
        <div>
            <button
                onClick={() => setShowMenu(!showMenu)}
                type="button"
                className="text-gray-400 hover:text-gray-700"
            >
                <span className="material-icons align-middle">menu</span>
            </button>
            {
                showMenu && <div className="bg-gray-100 p-2 fixed rounded-md shadow-xl">
                    {children}
                </div>
            }
        </div>
    )
}
