import React from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

export interface TextareaFieldProps {
    name: string;
    label: string;
    required: boolean;
    value: Date|null;
    error: string|undefined;
    valid: string|false|undefined;
    onChange: (e: any) => void;
}

export const Datepicker: React.FC<TextareaFieldProps> = ({ name, label, required, value, onChange, valid, error }) => {
    return (
        <div key={name} className="mb-2">
            <label className="mb-2" htmlFor={name}>
                { label }
                { required &&
                <small className="text-red-600 ml-1">*</small>
                }
            </label>
            <DatePicker
                selected={value ? new Date(value) : null}
                dateFormat="MMMM d, yyyy"
                className="border-2 rounded h-10 max-w-xs p-1"
                name="startDate"
                onChange={onChange}
            />
            <p className="mt-1 text-xs text-red-600 w-64">
                { valid && error}
            </p>
        </div>
    )
}
