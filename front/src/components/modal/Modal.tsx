import React from "react";

export interface ModalProps {
    children: React.ReactNode,
    showModal: boolean,
    closeModal: () => void;
}

export const Modal: React.FC<ModalProps> = ({ children, showModal, closeModal }) => {
    return (
        <div
            id="defaultModal"
            tabIndex={-1}
            aria-hidden="true"
            className="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full bg-black bg-opacity-50 grid h-screen place-items-center"
            onClick={closeModal}
        >
            <div
                onClick={(e) => e.stopPropagation()}
                className="bg-slate-100 w-fit p-10 rounded-md shadow-md"
            >
                <div className="flex justify-end">
                    <button type="button" onClick={closeModal}>
                        <span className="material-icons">close</span>
                    </button>
                </div>
                {children}
            </div>
        </div>
    )
}
