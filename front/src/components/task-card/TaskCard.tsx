import React, {useState} from "react";
import {Link} from "react-router-dom";
import {CardDropdown} from "../card-dropdown/CardDropdown";
import {ToDoListForm} from "../to-do-list-form/ToDoListForm";
import {Modal} from "../modal/Modal";
import ToDoList from "../../interfaces/ToDoList";
import Task from "../../interfaces/Task";
import {TaskForm} from "../task-form/TaskForm";
import {TaskModalContent} from "../task-modal-content/TaskModalContent";

export interface ToDoListCardProps {
    task: Task,
    toDoListId: string,
    handleDelete: (e: any) => void,
    handleEdit: (task: Task) => void
}

export const TaskCard: React.FC<ToDoListCardProps> = ({ task, toDoListId, handleDelete, handleEdit }) => {
    const [openModal, setOpenModal] = useState(false);
    const [openShowModal, setOpenShowModal] = useState(false);

    let isDeadlineReached = null;
    let isDeadlineClose = null;
    if (task?.deadlineDate) {
        const taskDeadline = new Date(task.deadlineDate);
        const currentDate = new Date();
        taskDeadline.setHours(0, 0, 0, 0);
        currentDate.setHours(0, 0, 0, 0);
        isDeadlineReached = currentDate.toString() === taskDeadline.toString();

        const threeDaysBeforeDeadline = new Date();
        threeDaysBeforeDeadline.setDate(currentDate.getDate() + 3);
        threeDaysBeforeDeadline.setHours(0, 0, 0, 0);
        isDeadlineClose = (threeDaysBeforeDeadline.getTime() - taskDeadline.getTime()) >= 0;
    }

    return (
        <div className="bg-white hover:bg-gray-100 p-6 rounded-lg shadow-md">
            <div className="flex justify-between">
                <h2 className="text-lg mr-2 mr-6 flex">
                    {
                        task.isCompleted
                        ? <del className="text-gray-600">{task.name}</del>
                        : task.name
                    }
                    { !task.isCompleted && isDeadlineReached !== null && isDeadlineReached &&
                        <span className="material-icons ml-2 text-red-600">error</span>
                    }
                    { !task.isCompleted && isDeadlineClose !== null && isDeadlineClose && !isDeadlineReached &&
                        <span className="material-icons ml-2 text-orange-600">warning</span>
                    }
                </h2>
                <CardDropdown>
                    <button
                        onClick={() => setOpenShowModal(true)}
                        type="button"
                        className="text-green-600 hover:text-green-700 flex align-center gap-2"
                    >
                        <span className="material-icons">visibility</span>
                        Show
                    </button>
                    { !task.isCompleted &&
                        <button
                            onClick={() => setOpenModal(true)}
                            type="button"
                            className="text-yellow-600 hover:text-yellow-700 flex align-center gap-2"
                        >
                            <span className="material-icons">edit</span>
                            Edit
                        </button>
                    }
                    <button
                        onClick={(e) => {
                            e.preventDefault();
                            handleDelete(task.id);
                        }}
                        type="button"
                        className="text-red-600 hover:text-red-700 flex align-center gap-2"
                    >
                        <span className="material-icons">delete</span>
                        Delete
                    </button>
                </CardDropdown>
            </div>
            { openModal &&
                <Modal showModal={openModal} closeModal={() => setOpenModal(() => !openModal)}>
                    <TaskForm
                        toDoListId={toDoListId}
                        task={task}
                        onSubmit={handleEdit}
                        closeModal={() => setOpenModal(() => false)}
                    />
                </Modal>
            }
            { openShowModal &&
                <Modal showModal={openShowModal} closeModal={() => setOpenShowModal(() => !openShowModal)}>
                    <TaskModalContent
                        taskId={task.id}
                    />
                </Modal>
            }
        </div>
    )
}
