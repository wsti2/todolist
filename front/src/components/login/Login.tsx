import { Form } from "../form/Form";
import { FormField } from "../form-field/FormField";
import { Button } from "../button/Button";
import React, {useEffect, useState} from "react";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {useFormik} from "formik";
import * as Yup from "yup";
import {LoginService} from "../../services/AuthService";
import {RegisterFormFields} from "../register/Register";
import UserLogin from "../../interfaces/UserLogin";

export interface UserLoginFields {
    email: string;
    password: string;
}

export const Login: React.FC = () => {
    const navigate = useNavigate();
    const location = useLocation();
    // @ts-ignore
    const from = location?.state?.from?.pathname || '/dashboard';

    useEffect(() => {
        if (localStorage.getItem('token')) {
            navigate('/dashboard', { replace: true })
        }
    }, []);

    const [errors, setErrors] = useState('');
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        } as RegisterFormFields,
        validationSchema: Yup.object({
            email: Yup.string()
                .email('Must be a valid email')
                .required('Required'),
            password: Yup.string()
                .min(8, 'Must be 8 characters or more')
                .max(64, 'Must be 64 characters or less')
                .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/, 'Must contain at least one lowercase' +
                    ' and one uppercase letter, one number and one special character (!@#$%^&*)')
                .required('Required'),
        }),
        async onSubmit(values: UserLoginFields) {
            setErrors('');
            const userLogin: UserLogin = {
                email: values.email,
                password: values.password
            };
            if (await LoginService(userLogin)) {
                navigate(from, { replace: true });
                return;
            }
            setErrors('An error occurred during submission.');
            return;
        }
    });

    return (
        <div className="h-full flex justify-center items-center">
            <div className="bg-white p-14 h-min rounded-md drop-shadow-2xl">
                <h1 className="text-3xl text-center font-bold mb-8">Login</h1>
                { errors &&
                    <div className="p-3 bg-red-500 rounded mb-3 text-center text-white">
                        {errors}
                    </div>
                }
                <Form onSubmit={formik.handleSubmit}>
                    <FormField
                        name="email"
                        label="Email"
                        type="email"
                        value={formik.values.email}
                        required={true}
                        error={formik.errors.email}
                        valid={formik.touched.email && formik.errors.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <FormField
                        name="password"
                        label="Password"
                        type="password"
                        value={formik.values.password}
                        required={true}
                        error={formik.errors.password}
                        valid={formik.touched.password && formik.errors.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <div className="grid grid-col gap-4">
                        <Button type="submit">
                            Login
                        </Button>
                        <span className="text-center">OR</span>
                        <Link to="/register" replace={true} className="text-center text-orange-600 hover:text-orange-700">
                            Go to register page
                        </Link>
                    </div>
                </Form>
            </div>
        </div>
    )
}
