import React from "react";

export interface TextareaFieldProps {
    name: string;
    label: string;
    value: string;
    required: boolean;
    error: string|undefined;
    valid: string|false|undefined;
    onChange: (e: any) => void;
}

export const TextareaField: React.FC<TextareaFieldProps> = ({ name, label, value, required, onChange, valid, error }) => {
    return (
        <div key={name} className="grid mb-3">
            <label className="mb-1" htmlFor={name}>
                {label}
                {required &&
                <small className="text-red-600 ml-1">*</small>
                }
            </label>
            <textarea
                id={name}
                className="border-2 rounded h-10 max-w-xs p-1"
                name={name}
                value={value}
                onChange={onChange}
                required={required}
            />
            <p className="mt-1 text-xs text-red-600 w-64">
                { valid && error}
            </p>
        </div>
    )
}
