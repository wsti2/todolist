import React, {ReactNode} from "react";

export interface FormProps {
    children: ReactNode | ReactNode[];
    onSubmit: any;
    autoComplete?: "on"|"off"
}

export const Form: React.FC<FormProps> = ({ children, onSubmit, autoComplete = "on" }) => {
    return (
        <form onSubmit={onSubmit} className="grid justify-center" autoComplete={autoComplete}>
            {children}
        </form>
    )
}
