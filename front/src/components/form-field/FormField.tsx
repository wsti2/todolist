import React, {ReactNode} from "react";

export interface FormFieldProps {
    name: string;
    label: string;
    type: string;
    value: string;
    required: boolean;
    error: string|undefined;
    valid: string|false|undefined;
    onChange: (e: React.FormEvent<HTMLInputElement>) => void;
    onBlur: (e: React.FormEvent<HTMLInputElement>) => void;
}

export const FormField: React.FC<FormFieldProps> = ({ name, label, type, value, required, onChange, onBlur, valid, error }) => {
    return (
        <div key={name} className="grid mb-3">
            <label className="mb-1" htmlFor={name}>
                {label}
                {required &&
                    <small className="text-red-600 ml-1">*</small>
                }
            </label>
            <input
                id={name}
                className="border-2 rounded h-10 max-w-xs p-1"
                type={type}
                name={name}
                value={value}
                onChange={onChange}
                onBlur={onBlur}
                required={required}
            />
            <p className="mt-1 text-xs text-red-600 w-64">
                { valid && error}
            </p>
        </div>
    )
}
