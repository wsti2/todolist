import React, {useEffect, useState} from "react";
import { Form } from '../form/Form'
import { FormField } from "../form-field/FormField";
import { Button } from "../button/Button";
import {Link, useNavigate} from "react-router-dom";
import {FormikHelpers, useFormik} from "formik";
import * as Yup from 'yup';
import {RegisterService} from "../../services/AuthService";
import UserRegister from "../../interfaces/UserRegister";

export interface RegisterFormFields {
    email: string;
    password: string;
    confirmPassword: string;
}

export const Register: React.FC = () => {
    const navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem('token')) {
            navigate('/dashboard', { replace: true })
        }
    }, []);

    const [errors, setErrors] = useState('');
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            confirmPassword: ''
        } as RegisterFormFields,
        validationSchema: Yup.object({
            email: Yup.string()
                .email('Must be a valid email')
                .required('Required'),
            password: Yup.string()
                .min(8, 'Must be 8 characters or more')
                .max(64, 'Must be 64 characters or less')
                .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/, 'Must contain at least one lowercase' +
                    ' and one uppercase letter, one number and one special character (!@#$%^&*)')
                .required('Required'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
        }),
        async onSubmit(values: RegisterFormFields, formikHelpers: FormikHelpers<RegisterFormFields>) {
            setErrors('');
            const userRegister: UserRegister = {
                email: values.email,
                password: values.password
            };
            if (await RegisterService(userRegister)) {
                navigate('/login', { replace: true });
                return true;
            }
            setErrors('An error occurred during submission.');
            return;
        }
    });

    return (
        <div className="h-full flex justify-center items-center">
            <div className="bg-white p-14 h-min rounded-md drop-shadow-2xl w-xs max-w-xs sm:max-w-md sm:w-md">
                <h1 className="text-3xl text-center font-bold mb-8">Register</h1>
                { errors &&
                    <div className="p-3 bg-red-500 rounded mb-3 text-center text-white">
                        {errors}
                    </div>
                }
                <Form onSubmit={formik.handleSubmit} autoComplete="off">
                    <FormField
                        name="email"
                        label="Email"
                        type="email"
                        value={formik.values.email}
                        required={true}
                        error={formik.errors.email}
                        valid={formik.touched.email && formik.errors.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <FormField
                        name="password"
                        label="Password"
                        type="password"
                        value={formik.values.password}
                        required={true}
                        error={formik.errors.password}
                        valid={formik.touched.password && formik.errors.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <FormField
                        name="confirmPassword"
                        label="Confirm password"
                        type="password"
                        value={formik.values.confirmPassword}
                        required={true}
                        error={formik.errors.confirmPassword}
                        valid={formik.touched.confirmPassword && formik.errors.confirmPassword}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <div className="grid grid-col gap-4">
                        <Button type="submit">
                            Register
                        </Button>
                        <span className="text-center">OR</span>
                        <Link to="/login" replace={true} className="text-center text-orange-600 hover:text-orange-700">
                            Go to login page
                        </Link>
                    </div>
                </Form>
            </div>
        </div>
    )
}
