import React from "react";

export interface TextareaFieldProps {
    name: string;
    label: string;
    required: boolean;
    error: string|undefined;
    valid: string|false|undefined;
    onChange: (e: any) => void;
}

export const CheckboxField: React.FC<TextareaFieldProps> = ({ name, label, required, onChange, valid, error }) => {
    return (
        <div key={name} className="mb-2">
            <input
                type="checkbox"
                id={name}
                name={name}
                onChange={onChange}
                required={required}
            />
            <label className="mb-2 ml-2" htmlFor={name}>
                { label }
                { required &&
                    <small className="text-red-600 ml-1">*</small>
                }
            </label>
            <p className="mt-1 text-xs text-red-600 w-64">
                { valid && error}
            </p>
        </div>
    )
}
