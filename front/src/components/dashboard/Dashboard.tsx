import React, {useEffect, useState} from "react";
import {Button} from "../button/Button";
import {Modal} from "../modal/Modal";
import ToDoList from "../../interfaces/ToDoList";
import {deleteToDoList, getToDoLists} from "../../services/ToDoListService";
import {ToDoListCard} from "../to-do-list-card/ToDoListCard";
import {ToDoListForm} from "../to-do-list-form/ToDoListForm";

export const Dashboard: React.FC = () => {
    const [lists, setLists] = useState<ToDoList[]>([]);
    const [openModal, setOpenModal] = useState(false);

    useEffect(() => {
        getToDoLists({ sortBy: 'createdAt', sortOrder: 'ASC' })
            .then((boards: ToDoList[]|boolean) => {
                if (Array.isArray(boards)) {
                    setLists(() => boards);
                }
            });
    }, []);

    const handleDelete = (toDoListId: string) => {
        deleteToDoList(toDoListId)
            .then((success) => {
                if (success) {
                    setLists(() => lists.filter((list) => list.id !== toDoListId))
                }
            });
    }

    const handleEdit = (list: ToDoList) => {
        getToDoLists({ sortBy: 'createdAt', sortOrder: 'ASC' })
            .then((lists: ToDoList[]|boolean) => {
                if (Array.isArray(lists)) {
                    setLists(() => lists);
                }
            });
    }

    return (
        <div className="p-6 lg:px-24">
            <div className="mb-4">
                <Button onClick={() => setOpenModal(!openModal)} type="button">
                    <span className="material-icons">add</span>
                    Add list
                </Button>
            </div>
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-5">
                {lists.map((list: ToDoList) =>
                    <ToDoListCard
                        handleEdit={handleEdit}
                        handleDelete={handleDelete}
                        key={list.id}
                        toDoList={list}
                    />
                )}
            </div>
            { openModal &&
                <Modal showModal={openModal} closeModal={() => setOpenModal(() => !openModal)}>
                    <ToDoListForm
                        onSubmit={(list: ToDoList) => setLists((lists) => [...lists, list])}
                        closeModal={() => setOpenModal(() => false)}
                    />
                </Modal>
            }
        </div>
    )
}
