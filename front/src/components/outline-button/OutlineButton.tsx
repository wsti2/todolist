import React, {ReactNode} from "react";
import { ButtonProps } from "../button/Button";

export interface OutlineButtonProps extends ButtonProps {
    type: "submit" | "button" | "reset" | undefined;
    children?: ReactNode | ReactNode[];
    onClick: () => void
}

export const OutlineButton: React.FC<OutlineButtonProps> = ({ type, children, onClick }: OutlineButtonProps) => {
    return (
        <button
            onClick={onClick}
            className="hover:bg-black/10 text-white p-3 rounded font-medium uppercase tracking-wider flex gap-2"
            type={type}
        >
            {children}
        </button>
    )
}
