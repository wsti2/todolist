import React, {useState} from "react";
import {Link} from "react-router-dom";
import {CardDropdown} from "../card-dropdown/CardDropdown";
import {ToDoListForm} from "../to-do-list-form/ToDoListForm";
import {Modal} from "../modal/Modal";
import ToDoList from "../../interfaces/ToDoList";

export interface ToDoListCardProps {
    toDoList: ToDoList,
    handleDelete: (e: any) => void
    handleEdit: (toDoList: ToDoList) => void
}

export const ToDoListCard: React.FC<ToDoListCardProps> = ({ toDoList, handleDelete, handleEdit }) => {
    const [openModal, setOpenModal] = useState(false);

    return (
        <div className="bg-white hover:bg-gray-100 p-6 rounded-lg shadow-md">
            <div className="flex justify-between">
                <h2 className="text-lg">{toDoList.name}</h2>
                <CardDropdown>
                    <Link to={'/to-do-lists/' + toDoList.id}>
                        <button
                            type="button"
                            className="text-green-600 hover:text-green-700 flex align-center gap-2"
                        >
                            <span className="material-icons">visibility</span>
                            Show
                        </button>
                    </Link>
                    <button
                        onClick={() => setOpenModal(true)}
                        type="button"
                        className="text-yellow-600 hover:text-yellow-700 flex align-center gap-2"
                    >
                        <span className="material-icons">edit</span>
                        Edit
                    </button>
                    <button
                        onClick={(e) => {
                            e.preventDefault();
                            handleDelete(toDoList.id);
                        }}
                        type="button"
                        className="text-red-600 hover:text-red-700 flex align-center gap-2"
                    >
                        <span className="material-icons">delete</span>
                        Delete
                    </button>
                </CardDropdown>
            </div>
            { openModal &&
                <Modal showModal={openModal} closeModal={() => setOpenModal(() => !openModal)}>
                    <ToDoListForm
                        toDoList={toDoList}
                        onSubmit={handleEdit}
                        closeModal={() => setOpenModal(() => false)}
                    />
                </Modal>
            }
        </div>
    )
}
