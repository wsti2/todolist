import Task from "../../interfaces/Task";
import React, {useEffect, useState} from "react";
import {Button} from "../button/Button";
import {Modal} from "../modal/Modal";
import {useParams} from "react-router-dom";
import {TaskForm} from "../task-form/TaskForm";
import {TaskCard} from "../task-card/TaskCard";
import {deleteTask, getTasks} from "../../services/TaskService";

export const ToDoList: React.FC = () => {
    const [tasks, setTasks] = useState<Task[]>([]);
    const [openModal, setOpenModal] = useState(false);

    const params = useParams<{ toDoListId: string }>();
    const toDoListId = params.toDoListId ?? '';


    useEffect(() => {
        getTasks({ sortBy: 'createdAt', sortOrder: 'ASC' })
            .then((tasks: Task[]|boolean) => {
                if (Array.isArray(tasks)) {
                    setTasks(() => sortTasks(tasks));
                }
            });
    }, []);

    const handleDelete = (toDoListId: string) => {
        deleteTask(toDoListId)
            .then((success) => {
                if (success) {
                    setTasks(() => tasks.filter((task) => task.id !== toDoListId))
                }
            });
    }

    const handleEdit = (task: Task) => {
        getTasks({ sortBy: 'createdAt', sortOrder: 'ASC' })
            .then((tasks: Task[]|boolean) => {
                if (Array.isArray(tasks)) {
                    setTasks(() => sortTasks(tasks));
                }
            });
    }

    const sortTasks = (tasks: Task[]): Task[] => {
        return tasks.sort((a, b) => {
            if (a.isCompleted === b.isCompleted) return 0;
            return a.isCompleted ? 1 : -1;
        });
    };

    return (
        <div className="p-6 lg:px-24">
            <div className="mb-4">
                <Button onClick={() => setOpenModal(true)} type="button">
                    <span className="material-icons">add</span>
                    Add task
                </Button>
            </div>
            <div  className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-5">
                {tasks.map((task, index, array) =>
                    <TaskCard
                        handleEdit={handleEdit}
                        handleDelete={handleDelete}
                        key={task.id}
                        task={task}
                        toDoListId={toDoListId}
                    />
                )}
            </div>
            { openModal &&
            <Modal showModal={openModal} closeModal={() => setOpenModal(() => !openModal)}>
                <TaskForm
                    toDoListId={toDoListId}
                    onSubmit={(task: Task) => setTasks((tasks) => [...tasks, task])}
                    closeModal={() => setOpenModal(() => false)}
                />
            </Modal>
            }
        </div>
    );
}
