import React from "react";
import { OutlineButton } from "../outline-button/OutlineButton";
import {Link, useNavigate} from "react-router-dom";
import {LogoutService} from "../../services/AuthService";

export const Menu: React.FC = () => {
    const navigator = useNavigate();
    const handleLogout = () => {
        LogoutService();
        navigator('/login', { replace: true })
    }

    return (
        <nav className="bg-gradient-to-r from-orange-400 to-orange-600 flex justify-between p-4 lg:px-20">
            <Link to="/dashboard" className="flex items-center">
                <h1 className="font-bold text-2xl sm:text-4xl text-white leading-tight">
                    To Do List
                </h1>
            </Link>
            <div>
                <OutlineButton onClick={handleLogout} type="button">
                    <span className="material-icons">logout</span>
                    Logout
                </OutlineButton>
            </div>
        </nav>
    )
}
