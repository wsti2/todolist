import {Navigate, Outlet, useLocation} from "react-router-dom";
import jwtDecode from "jwt-decode";

export const RequireAuth = () => {
    const token = localStorage.getItem('token');
    const location = useLocation();

    return (
        token
            ? <Outlet />
            : <Navigate to="/login" state={{ from: location }} replace={true} />
    )
}
