import axios from "axios";
import {LogoutService, RefreshToken} from "../services/AuthService";

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8000/api'
});

axiosInstance.interceptors.request.use((config) => {
    config.headers = {
        'Accept': 'application/json',
        'Content-Type': config?.headers?.['Content-Type'] ?? 'application/json'
    };
    if (localStorage.getItem('token')) {
        config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
    }
    return config;
});

axiosInstance.interceptors.response.use(response => {
    return response;
}, async (error) => {
    if (error.config.url === '/token/refresh') {
        LogoutService();
    }
    if (error.response.status === 401 && localStorage.getItem('refreshToken')) {
        await RefreshToken();
    }
    return error;
});

export default axiosInstance;
