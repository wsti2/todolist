import axios from "../api/axios";
import ToDoList from "../interfaces/ToDoList";
import ToDoListFormData from "../interfaces/ToDoListFormData";

const TO_DO_LISTS_URL = '/todo_lists'

export const getToDoList = async (toDoListId: string): Promise<ToDoList|boolean> => {
    try {
        const response = await axios.get(`${TO_DO_LISTS_URL}/${toDoListId}`, {});
        if (response.status === 200) {
            return response.data as ToDoList;
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const getToDoLists = async (params: { [key: string]: string; }): Promise<ToDoList[]|boolean> => {
    try {
        let query = '';
        for (let paramName in params) {
            query += `${paramName}=${params[paramName]}&`;
        }
        const response = await axios.get(`${TO_DO_LISTS_URL}?${query}`, {});
        if (response.status === 200) {
            return response.data as ToDoList[];
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const createToDoList = async (stateData: ToDoListFormData|ToDoList): Promise<ToDoList|boolean> => {
    try {
        const response = await axios.post<ToDoList>(`${TO_DO_LISTS_URL}`, JSON.stringify({ ...stateData }));
        if (response.status === 201) {
            return response.data;
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const editToDoList = async (toDoList: ToDoListFormData|ToDoList): Promise<ToDoList|boolean> => {
    try {
        const response = await axios.patch(
            `${TO_DO_LISTS_URL}/${toDoList.id}`,
            { ...toDoList },
            { headers: { 'Content-Type': 'application/merge-patch+json' } }
        );
        if (response.status === 200) {
            return response.data as ToDoList;
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const deleteToDoList = async (toDoListId: string): Promise<ToDoList|boolean> => {
    try {
        const response = await axios.delete<ToDoList>(`${TO_DO_LISTS_URL}/${toDoListId}`);
        return response.status === 204;
    } catch (e) {
        return false;
    }
}
