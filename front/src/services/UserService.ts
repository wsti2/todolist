import axios from "../api/axios";
import User from "../interfaces/User";

const USER_URL = '/api/users';

export const getUsers = async (params: { [key: string]: string; }): Promise<User[]|boolean> => {
    try {
        let query = '';
        for (let paramName in params) {
            query += `${paramName}=${params[paramName]}&`;
        }
        const response = await axios.get(`${USER_URL}?${query}`, {});
        if (response.status === 200) {
            return response.data as User[];
        }
        return false;
    } catch (e) {
        return false;
    }
}
