import axios from "../api/axios";
import User from "../interfaces/User";
import UserRegister from "../interfaces/UserRegister";
import UserLogin from "../interfaces/UserLogin";
import Token from "../interfaces/Token";

export const RegisterService = async (userRegister: UserRegister) => {
    const registerUrl = '/users';
    try {
        const response = await axios.post<User>(
            registerUrl,
            JSON.stringify({ ...userRegister }),
            {
                headers: { 'Content-Type': 'application/json'}
            }
        );
        return response.status === 201;
    } catch (err) {
        return false;
    }
}

export const LoginService = async (userLogin: UserLogin): Promise<boolean> => {
    const loginUrl = '/token';
    try {
        const response = await axios.post<Token>(
            loginUrl,
            JSON.stringify({ ...userLogin }),
            {
                headers: { 'Content-Type': 'application/json'}
            }
        );
        if (response.data?.token) {
            console.log(response.data);
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('refreshToken', response.data.refresh_token);
            return true;
        }
        return false;
    } catch (err) {
        return false;
    }
}

export const RefreshToken = async (): Promise<boolean> => {
    const refreshTokenUrl = '/token/refresh';
    try {
        const response = await axios.post<Token>(
            refreshTokenUrl,
            JSON.stringify({ refresh_token: localStorage.getItem('refreshToken') }),
            {
                headers: { 'Content-Type': 'application/json'}
            }
        );
        if (response.data?.token && response.data?.refresh_token) {
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('refreshToken', response.data.refresh_token);
            return true;
        }
        return false;
    } catch (err) {
        return false;
    }
}

export const LogoutService = (): void => {
    localStorage.clear();
}
