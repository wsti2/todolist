import Task from "../interfaces/Task";
import axios from "../api/axios";
import TaskFormData from "../interfaces/TaskFormData";
import {deleteToDoList} from "./ToDoListService";

const TASKS_URL = '/tasks'

export const getTask = async (taskId: string): Promise<Task|boolean> => {
    try {
        const response = await axios.get(`${TASKS_URL}/${taskId}`, {});
        if (response.status === 200) {
            return response.data as Task;
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const getTasks = async (params: { [key: string]: string; }): Promise<Task[]|boolean> => {
    try {
        let query = '';
        for (let paramName in params) {
            query += `${paramName}=${params[paramName]}&`;
        }
        const response = await axios.get(`${TASKS_URL}?${query}`, {});
        if (response.status === 200) {
            return response.data as Task[];
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const createTask = async (task: TaskFormData|Task): Promise<Task|boolean> => {
    try {
        const response = await axios.post<Task>(`${TASKS_URL}`, JSON.stringify({ ...task }));
        if (response.status === 201) {
            return response.data;
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const editTask = async (task: TaskFormData|Task): Promise<Task|boolean> => {
    try {
        const response = await axios.patch(
            `${TASKS_URL}/${task.id}`,
            { ...task },
            { headers: { 'Content-Type': 'application/merge-patch+json' } }
        );
        if (response.status === 200) {
            return response.data as Task;
        }
        return false;
    } catch (e) {
        return false;
    }
}

export const deleteTask = async (taskId: string): Promise<Task|boolean> => {
    try {
        const response = await axios.delete<Task>(`${TASKS_URL}/${taskId}`);
        return response.status === 204;
    } catch (e) {
        return false;
    }
}
