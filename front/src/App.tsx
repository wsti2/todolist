import {Outlet} from 'react-router-dom';
import React from "react";
import {Menu} from "./components/menu/Menu";

export const App: React.FC = () => {
    return (
        <div className="flex flex-col h-screen">
            <Menu />
            <Outlet />
        </div>
    )
}
