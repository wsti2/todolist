<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\TodoList;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

final class ToDoListSubscriber implements EventSubscriberInterface
{
    public function __construct(private Security $security)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['assignOwner', EventPriorities::PRE_VALIDATE],
        ];
    }

    public function assignOwner(ViewEvent $event): void
    {
        $requestMethod = $event->getRequest()->getMethod();
        $toDoList = $event->getControllerResult();

        if (!$toDoList instanceof TodoList || Request::METHOD_POST !== $requestMethod) {
            return;
        }

        $currentUser = $this->security->getUser();

        if (!$currentUser) {
            return;
        }

        $toDoList->setOwner($currentUser);

    }
}
