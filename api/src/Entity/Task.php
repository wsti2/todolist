<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\TaskRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
#[ApiResource]
#[Get(
    normalizationContext: ['groups' => ['task:read']],
    security: 'object.getTodoList().getOwner().getId() == user.getId()'
)]
#[GetCollection(
    normalizationContext: ['groups' => ['task:read']],
)]
#[Post(
    normalizationContext: ['groups' => ['task:read']],
    denormalizationContext: ['groups' => ['task:create']]
)]
#[Patch(
    normalizationContext: ['groups' => ['task:read']],
    denormalizationContext: ['groups' => ['task:update']],
    security: 'object.getTodoList().getOwner().getId() == user.getId()'
)]
#[Delete(
    security: 'object.getTodoList().getOwner().getId() == user.getId()'
)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('task:read')]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'tasks')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    #[Groups(['task:create', 'task:read'])]
    private ?TodoList $todoList = null;

    #[ORM\Column(length: 64)]
    #[Assert\NotBlank, Assert\Length(min: 3, max: 64)]
    #[Groups(['task:create', 'task:update', 'task:read'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['task:create', 'task:update', 'task:read'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['task:create', 'task:update', 'task:read'])]
    private ?bool $isCompleted = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['task:create', 'task:update', 'task:read'])]
    private ?\DateTimeInterface $deadlineDate = null;

    #[ORM\Column]
    #[Groups('task:read')]
    private ?\DateTimeImmutable $createdAt = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTodoList(): ?TodoList
    {
        return $this->todoList;
    }

    public function setTodoList(?TodoList $todoList): self
    {
        $this->todoList = $todoList;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    public function getDeadlineDate(): ?\DateTimeInterface
    {
        return $this->deadlineDate;
    }

    public function setDeadlineDate(?\DateTimeInterface $deadlineDate): self
    {
        $this->deadlineDate = $deadlineDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
