<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\TodoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TodoListRepository::class)]
#[ApiResource]
#[Get(
    normalizationContext: ['groups' => ['todo-list:read']],
    security: 'object.getOwner().getId() == user.getId()'
)]
#[GetCollection(
    normalizationContext: ['groups' => ['todo-list:read']],
)]
#[Post(
    normalizationContext: ['groups' => ['todo-list:read']],
    denormalizationContext: ['groups' => ['todo-list:create']]
)]
#[Patch(
    normalizationContext: ['groups' => ['todo-list:read']],
    denormalizationContext: ['groups' => ['todo-list:update']],
    security: 'object.getOwner().getId() == user.getId()'
)]
#[Delete(
    security: 'object.getOwner().getId() == user.getId()'
)]
class TodoList
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['todo-list:read'])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'todoLists')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    #[Groups(['todo-list:create', 'todo-list:read'])]
    private ?User $owner = null;

    #[ORM\Column(length: 64)]
    #[Assert\NotBlank, Assert\Length(min: 3, max: 64)]
    #[Groups(['todo-list:create', 'todo-list:update', 'todo-list:read'])]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'todoList', targetEntity: Task::class, orphanRemoval: true)]
    private Collection $tasks;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Task>
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks->add($task);
            $task->setTodoList($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->removeElement($task)) {
            if ($task->getTodoList() === $this) {
                $task->setTodoList(null);
            }
        }

        return $this;
    }
}
