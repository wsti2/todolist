<?php

namespace App\Doctrine\CurrentUserExtension\Strategies;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

final class CurrentUserForTodoListStrategy implements CurrentUserStrategyInterface
{
    public function applyExtension(QueryBuilder $queryBuilder, UserInterface $user): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere(sprintf('%s.owner = :currentUser', $rootAlias));
        $queryBuilder->setParameter('currentUser', $user->getId());
    }
}
