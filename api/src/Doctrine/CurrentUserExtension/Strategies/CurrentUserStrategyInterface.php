<?php

namespace App\Doctrine\CurrentUserExtension\Strategies;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

interface CurrentUserStrategyInterface
{
    public function applyExtension(QueryBuilder $queryBuilder, UserInterface $user): void;
}
