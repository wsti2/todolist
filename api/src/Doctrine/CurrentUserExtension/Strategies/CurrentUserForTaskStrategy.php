<?php

namespace App\Doctrine\CurrentUserExtension\Strategies;

use App\Entity\TodoList;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

final class CurrentUserForTaskStrategy implements CurrentUserStrategyInterface
{
    public function applyExtension(QueryBuilder $queryBuilder, UserInterface $user): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->leftJoin(TodoList::class, 't', Join::WITH, sprintf('%s.todoList = t.id', $rootAlias));
        $queryBuilder->andWhere('t.owner = :currentUser');
        $queryBuilder->setParameter('currentUser', $user->getId());
    }
}
