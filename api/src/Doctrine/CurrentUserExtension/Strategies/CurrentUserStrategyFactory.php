<?php

namespace App\Doctrine\CurrentUserExtension\Strategies;

use App\Entity\Task;
use App\Entity\TodoList;

final class CurrentUserStrategyFactory
{
    public function create(string $class): CurrentUserStrategyInterface
    {
        return match ($class) {
            TodoList::class => new CurrentUserForTodoListStrategy(),
            Task::class => new CurrentUserForTaskStrategy(),
        };
    }
}
