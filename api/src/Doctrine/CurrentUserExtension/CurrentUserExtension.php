<?php

namespace App\Doctrine\CurrentUserExtension;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Doctrine\CurrentUserExtension\Strategies\CurrentUserStrategyFactory;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

final class CurrentUserExtension implements QueryCollectionExtensionInterface
{
    public function __construct(
        private Security $security,
        private CurrentUserStrategyFactory $currentUserStrategyFactory
    )
    {
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []
    ): void
    {
        $user = $this->security->getUser();

        if ($user === null) {
            return;
        }

        $currentUserStrategy = $this->currentUserStrategyFactory->create($resourceClass);
        $currentUserStrategy->applyExtension($queryBuilder, $user);
    }
}
